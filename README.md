# Docker Compose #

Die Docker Compose referenziert die Repositorien von Bitbucket.
Ohne das Herunterladen der Sourcen werden mit 'docker-compose up' die drei Prototypen gebuildet

* Python Flask Backend
* Vue Frontend - Builder Tool
* Geoerver

Soll der Geoserver lokal gebuildet werden, müssen das Backend und Frontend den folgenden Code enthalten:


```
args:                                                                      
            GEOSERVER_LOCAL: TRUE
```

Für einen gewissen Zeitraum sollte der Test-GeoServer der Prototypisierungsphase genutzt werden können:

https://csl-lig.hcu-hamburg.de/geoserver/web/

In diesem Fall muss der Geoserver aus den Services der Compose entfernt werden.

Für das Builden auf lokalen Sourcen ist die docker-compose-local.yml im Repository enhalten.
